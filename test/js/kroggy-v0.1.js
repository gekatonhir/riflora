﻿/// Super ugly code below. Parse at your own peril!
/// Extremely beta version to merely get things done. Optimization and rest of the hoopla to come later
/// Kroggy v0.1
/// ~Sid

$(function(){
	var progress = $('#progress'),
	progressKeeper = $('#progressKeeper'),
	notice = $("#notice"),
	progressWidth = 548,
	answers= kroggy.answers,
	userAnswers = [],
	questionLength= answers.length,
	questionsStatus = $("#questionNumber")
	questionsList = $(".question");

 function checkAnswers() {
    var resultArr = [],
				flag = false;
    for (i=0; i<answers.length; i++) {

        if (answers[i] == userAnswers[i]) {
            flag = true;
        }
        else {
            flag = false;
        }
        resultArr.push(flag);
    }
    return resultArr;
  }

  function roundReloaded(num, dec) {
		var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
		return result;
	}

	function judgeSkills(score) {
		var returnString;

		    if (score>90) returnString = '<div class="res90"><h2 class="good90">Вы случайно не врач?</h2> <br /> Поздравляем, вы отлично знакомы с правилами выживания в отпуске. <br /> Максимально приблизить его к отпуску мечты позволят несколько простых привычек, которые стоит начать практиковать уже сейчас. Попробуйте - и ваш организм скажет вам спасибо.</div>'

			else if (score>49) returnString = '<div class="res50"><h2 class="good50">Вы в ладах с собественным здоровьем</h2> <br /> Поздравляем, но все же кое-какие нюансы от вас ускользнули. Специально для вас мы собрали несколько полезных советов, которые помогут подготовить организм к поездке!</div>'

			else returnString = '<div class="res20"><h2 class="good20">Вы совсем не подозреваете как вести себя в отпуске и как к нему подготовиться!</h2> <br /> Специально для вас мы подготовили несколько советов, которые помогут вам подружиться с соб здоровьем и провести отпуск именно так, как было задумано.</div>'

		return returnString;
	}

	  progressKeeper.hide();
	  notice.hide();
	  $("#main-quiz-holder input:radio").attr("checked", false);

		$('.answers li input').click(function() {
				$(this).parents('.answers').children('li').removeClass("selected");
					$(this).parents('li').addClass('selected');

			});


	  $('.btnStart').click(function(){

        $(this).parents('.questionContainer').fadeOut(500, function(){
            $(this).next().fadeIn(500, function(){ progressKeeper.show(); });
        });

			 questionsStatus.text("");
			 return false;

    });

    $('.btnNext').click(function(){

			var tempCheck = $(this).parents('.questionContainer').find('input[type=radio]:checked');
        if (tempCheck.length == 0) {
             notice.fadeIn(300);return false;
        }
			 notice.hide();
        $(this).parents('.questionContainer').fadeOut(500, function(){
            $(this).next().fadeIn(500);
        });
        progress.animate({ width: progress.width() + Math.round(progressWidth/questionLength), }, 500 );
			 return false;
    });

    $('.btnPrev').click(function(){
			notice.hide();
        $(this).parents('.questionContainer').fadeOut(500, function(){
            $(this).prev().fadeIn(500)
        });

        progress.animate({ width: progress.width() - Math.round(progressWidth/questionLength), }, 500 );
			 return false;
    });

    $('.btnShowResult').click(function(){

			var tempCheck = $(this).parents('.questionContainer').find('input[type=radio]:checked');
        if (tempCheck.length == 0) {
             notice.fadeIn(300);return false;
        }
        var tempArr = $('input[type=radio]:checked');
        for (var i = 0, ii = tempArr.length; i < ii; i++) {
            userAnswers.push(tempArr[i].getAttribute('data-key'));
        }

        progressKeeper.hide();
        var results = checkAnswers(),
			 		  resultSet = '',
					  trueCount = 0,
					  score;
        for (var i = 0, ii = results.length; i < ii; i++){
            if (results[i] == true) trueCount++;
            resultSet += '<div class="resultRow"> Вопрос #' + (i + 1) + (results[i]== true ? "<div class='correct'><span>Правильно!</span></div>": "<div class='wrong'><span>Неправильно!</span></div>") + "</div>";

        }
			 score =  roundReloaded(trueCount , 2);

        resultSet = '<div class="youres">Ваш результат '+score+' из 10</div>' +judgeSkills(score);
        $('#resultKeeper').html(resultSet).show();

			 $(this).parents('.questionContainer').fadeOut(500, function(){
            $(this).next().fadeIn(500);
        });
			 return false;
    });

})