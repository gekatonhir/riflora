<style>
table {margin:30px auto}
th,td {border:1px solid #e5e5e5;padding:15px}
.logo, .info .name, .info .region, .info .address {display:none}
</style>
<?php
$filename = 'data.json';
$file = file_get_contents($filename);
$res = json_decode($file, true);

echo '<table><thead><tr><th>id</th><th>coordinates</th><th>clusterCaption</th><th>hintContent</th><th>balloonContent</th></tr></thead><tbody>';
$i = 0;
foreach($res['features'] as $val) {
	$i++;
	echo '<tr>';
	echo '<td>'.$val['id'].'</td>';
	echo '<td>'.$val['geometry']['coordinates'][0].','.$val['geometry']['coordinates'][1].'</td>';
	echo '<td>'.$val['properties']['clusterCaption'].'</td>';
	echo '<td>'.$val['properties']['hintContent'].'</td>';
	echo '<td>'.$val['properties']['balloonContent'].'</td>';
	echo '</tr>';
}
echo '</tbody></table>';
?>