﻿<?php
    @ini_set("output_buffering", "Off");
    @ini_set('implicit_flush', 1);
    @ini_set('zlib.output_compression', 0);
    @ini_set('max_execution_time', 1800);

    $sbm = $_POST['sbm'];
    
    if(isset($sbm))
    {
        $all_string = $_POST['all_string'];
        
        if($all_string != null)
        {
            $all_string = explode("\n", $all_string);
            
            $noempty = 0;
            $empty = 0;
            
            foreach($all_string as $als)
            {
                (trim($als) != "") ? $noempty++ : $empty++;
            }
            
            echo "Итого всего строк: ".count($all_string).", из которых пустых: ".$empty.", заполненных: ".$noempty."<br><br><b>Точки геолокации:</b><br>";
            
            echo '<ol>';
            foreach($all_string as $key=>$als)
            {
				$xml = simplexml_load_file("https://geocode-maps.yandex.ru/1.x/?geocode=".$als."");
				$pos = $xml->GeoObjectCollection->featureMember->GeoObject->Point->pos;
				$position = explode(' ',$pos,2);
				$result = $position[1].",".$position[0];
                echo "<li>".$result."</li>"; //Вывод точек геолокации
            }
            echo '</ol>';
            
        }
        else
        {
            echo "<b>Данные не введены!</b>";
        }
    }
?>
<br/>
<form action="" method="post">
    Введите каждый адрес с новой строки<br>
    <textarea name="all_string" style="height:200px; width:600px;" wrap="off"></textarea>
    <input type="submit" value="ОК" name="sbm">
</form>