<?php
$row = 0;
if (($handle = fopen("table.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
		$n = $row - 1;
        echo "
        {\"type\": \"Feature\", \"id\": ".$n.", \"geometry\": {\"type\": \"Point\", \"coordinates\": [".$data[4]."]}, \"properties\": {\"balloonContent\": \"<img class='logo' src='' alt='".$data[0]."' /><div class='info'><span class='name'>".$data[0]."</span><span class='region'>".$data[1]."</span><span class='address'>".$data[2]."</span><span class='phone'>".$data[3]."</span></div>\", \"clusterCaption\": \"".$data[0]."\", \"hintContent\": \"".$data[2]."\"}},";
    }
    fclose($handle);
}
?>