$(document).ready(function(){

	$('.burger').on("click", function(){
		$('.nav').toggleClass("active");
	});

	$('.btn_video').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        helpers : {
            media : {}
        }
    });


	$('a[href*=#]').bind("click", function(e){
		var anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: $(anchor.attr('href')).offset().top - 130
		}, 1000);
			$('.nav').removeClass("active");
		e.preventDefault();
	});
	return false;
});

$(window).load(function() {

	$(".accordion .accord-header").click(function() {
		if($(this).next("div").is(":visible")){
			$(this).next("div").slideUp("slow");
			advices();
		} else {
			$(".accordion .accord-content").slideUp("slow");
			$(this).next("div").slideToggle("slow");
			advices();
		}
	});

	advices();

 });

function advices() {
var list = $('.advice_wrap');
    list.bxSlider({
        adaptiveHeight: true,
        pager: false,
        auto: false,
        pause: 5000,
        autoHover: true,
        slideWidth: '1000'
    })
}
/*****************************************/
function modalShowNexSlide() {

	$('#modal-second-slide').css("visibility", "visible");
	$('#modal-first-slide').css("visibility", "hidden");

}